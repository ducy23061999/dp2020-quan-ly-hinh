import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Đọc danh sách các hình từ file
 * 
 * @author nvtrung
 *
 */
public class FileHìnhReader implements IHìnhReader {
	private String filename;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public FileHìnhReader(String filename) {
		this.filename = filename;
	}

	@Override
	public List<Hình> nạpDanhSáchHình() {
		List<Hình> lst = new ArrayList<Hình>();

		// Đọc danh sách các hình từ file "hinh.txt" --> lst
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(this.getFilename()));
			while (true) {
				String line = br.readLine();
				if (line == null)
					break;

				if (line.startsWith("#"))
					continue;

//				#1|cạnh_1|cạnh_2|cạnh_3
//				#2|bánkính
//				#3|cạnh_1|cạnh_2
				String[] info = line.split("\\|");
				if (info[0].equals("1")) {
					// tạo ra hình tam giác
					double a = Double.parseDouble(info[1]);
					double b = Double.parseDouble(info[2]);
					double c = Double.parseDouble(info[3]);
					HìnhTamGiác x = new HìnhTamGiác(a, b, c);
					lst.add(x);

				} else if (info[0].equals("2")) {
					// tạo ra hình tròn
					double r = Double.parseDouble(info[1]);
					HìnhTròn x = new HìnhTròn(r);
					lst.add(x);
				} else {
					// tạo ra hình chữ nhật
					double a = Double.parseDouble(info[1]);
					double b = Double.parseDouble(info[2]);
					HìnhChữNhật x = new HìnhChữNhật(a, b);
					lst.add(x);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return lst;
	}
	
}
