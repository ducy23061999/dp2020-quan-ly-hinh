import java.util.List;

public interface IHìnhReader {
	
	/**
	 * nạp danh sách các đối tượng Hình và trả về danh sách này
	 * @return
	 */
	public List<Hình> nạpDanhSáchHình();
}
