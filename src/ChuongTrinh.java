import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ChuongTrinh {
	static void thuNghiem() {
//		Hình x;
//
//		x = new HìnhTròn();
//		((HìnhTròn) x).setBánKính(10.5);
//
//		x = new HìnhTamGiác();
//		((HìnhTamGiác) x).setA(10);
//		((HìnhTamGiác) x).setB(1);
//		((HìnhTamGiác) x).setC(10);
//
//		System.out.println(x.getInfo());
	}

//	static double getChuVi(Hình x) {
//		double cv = -1;
//		if (x instanceof HìnhTròn)
//			cv = ((HìnhTròn) x).getBánKính() * 2 * 3.141592;
//		else if (x instanceof HìnhTamGiác) {
//			HìnhTamGiác xx = (HìnhTamGiác) x;
//			cv = xx.getA() + xx.getB() + xx.getC();
//		} else {
//			HìnhChữNhật xx = (HìnhChữNhật) x;
//			cv = (xx.getChiềuDài() + xx.getChiềuRộng()) * 2;
//		}
//		return cv;
//	}
	
//	static List<Hình> loadFromFile(){
//		List<Hình> lst = new ArrayList<Hình>();
//
//		// Đọc danh sách các hình từ file "hinh.txt" --> lst
//		BufferedReader br = null;
//		try {
//			br = new BufferedReader(new FileReader("hinh.txt"));
//			while (true) {
//				String line = br.readLine();
//				if (line == null)
//					break;
//
//				if (line.startsWith("#"))
//					continue;
//
////				#1|cạnh_1|cạnh_2|cạnh_3
////				#2|bánkính
////				#3|cạnh_1|cạnh_2
//				String[] info = line.split("\\|");
//				if (info[0].equals("1")) {
//					// tạo ra hình tam giác
//					double a = Double.parseDouble(info[1]);
//					double b = Double.parseDouble(info[2]);
//					double c = Double.parseDouble(info[3]);
//					HìnhTamGiác x = new HìnhTamGiác(a, b, c);
//					lst.add(x);
//
//				} else if (info[0].equals("2")) {
//					// tạo ra hình tròn
//					double r = Double.parseDouble(info[1]);
//					HìnhTròn x = new HìnhTròn(r);
//					lst.add(x);
//				} else {
//					// tạo ra hình chữ nhật
//					double a = Double.parseDouble(info[1]);
//					double b = Double.parseDouble(info[2]);
//					HìnhChữNhật x = new HìnhChữNhật(a, b);
//					lst.add(x);
//				}
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (br != null)
//					br.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		return lst;
//	}
		
	public static void main(String[] args) {
//		thuNghiem();

		IHìnhReader reader = null;
		
		
//		reader = new FileHìnhReader("hinh.txt");
		reader = new MySQLHìnhReader();
		
		List<Hình> lst = reader.nạpDanhSáchHình();

		// Câu 1 - Hiển thị các hình
		for (int i = 0; i < lst.size(); i++)
			System.out.println(String.format("%d. %s", i + 1, lst.get(i).getInfo()));

		// Tìm chu vi lớn nhất --> maxCV
		double cvMax = -1;
		for (Hình x : lst) {
			double cv = x.getChuVi();
			if (cv > cvMax)
				cvMax = cv;
		}		
		System.out.println("Chu vi lớn nhất của các hình là " + cvMax);

		// In các hình có chu vi = maxCV
		int nMax = 0;
		for (int i = 0; i < lst.size(); i++)
			if (lst.get(i).getChuVi() == cvMax) {
				nMax++;
				System.out.println(String.format("%d. %s", nMax, lst.get(i).getInfo()));
			}
	}
}
