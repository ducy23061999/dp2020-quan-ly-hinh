
public class HìnhChữNhật extends Hình {
	public double getChiềuDài() {
		return chiềuDài;
	}

	public void setChiềuDài(double chiềuDài) {
		this.chiềuDài = chiềuDài;
	}

	public double getChiềuRộng() {
		return chiềuRộng;
	}

	public HìnhChữNhật(double chiềuDài, double chiềuRộng) {
		this.chiềuDài = chiềuDài;
		this.chiềuRộng = chiềuRộng;
	}

	public void setChiềuRộng(double chiềuRộng) {
		this.chiềuRộng = chiềuRộng;
	}

	private double chiềuDài;
	private double chiềuRộng;

	@Override
	public String getInfo() {
		return String.format("Hình chữ nhật %5.2f x %5.2f", this.getChiềuDài(), this.getChiềuRộng());
	}
	
	public double getChuVi() {
		return (this.getChiềuDài() + this.getChiềuDài())*2;
	}

}
